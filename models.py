from datetime import datetime, timedelta
from urllib.parse import urlparse


class PollerData:
    """
    Contains all of the server's sites' information.
    """
    def __init__(self, urlRetriever, config):
        self.urlRetriever = urlRetriever
        self.config = config
        self.lastEntriesUpdate = None
        self.sites = {}

        self._updateEntries()

    def updateSite(self, url, content, outputwriter):
        """
        Update a site with the newly given information.
        """
        return self.sites[url].update(content, outputwriter)

    def getSitesUrls(self, update=True):
        """
        Return the (eventually updated) list of sites,
        in the form of a list of urls.
        """
        self._updateEntries()
        return [url for url in self.sites]

    def _updateEntries(self):
        """
        Updates the list of sites to watch, if the waiting time between
        urls reloading specified in the config file has been reached.
        """
        delay = int(self.config.get('URL_RELOAD_TIME', 30000))

        if not self.lastEntriesUpdate or \
                self.lastEntriesUpdate + timedelta(milliseconds=delay) \
                <= datetime.now():

            for url in self.urlRetriever.getUrlList(self.config):
                if url not in self.sites:
                    self.sites[url] = Site(url)
            self.lastEntriesUpdate = datetime.now()


class Site:
    """
    Contains the relevant information about a site.
    """
    def __init__(self, url):
        """
        Create a new entry for a site.
        url: url of the site to watch.
        """
        self.url = url
        self.changeTimes = [datetime.now()]
        self.currentContent = None
        self.currentHash = None

    def update(self, content, outputwriter):
        """
        Update the information about this site, and if needed, output changes.
        """
        hashedValue = hash(content)
        if hashedValue != self.currentHash:
            self.changeTimes.append(datetime.now())

            outputwriter.printNotification(
                self.url,
                self.changeTimes[-1],
                self.currentHash is None
            )

            self.currentHash = hashedValue
            self.currentContent = content

            name = urlparse(self.url).netloc
            outputwriter.printDiff(name, content)
            return True
        return False