Needs Python 3, but has only been tested with Python 3.4.

Launch server using command:
    python server.py
or
    python server.py configurationFile

Default configuration file is set as 'config.txt'.
If the configuration file is not found, a new one will be generated,
but it will not have the comments explaining what every setting does.

To know more about the possible configurations,
check the default configuration file.