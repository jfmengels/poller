import configparser
import os.path
from sys import stderr


class ConfigurationReader:
    """
    Reads or generates configuration files.
    """

    def read(self, filename):
        """
        Read the configuration file specified by filename.
        If the file could not be opened, a new one will be generated at the
        given location.
        """
        # If file does not exist, generate a new one and use default values.
        if not os.path.isfile(filename):
            return self.createDefaultConfigFile(filename)['DEFAULT']

        # Read config file
        configuration = configparser.ConfigParser()
        configuration.read(filename)

        # Add the default values if some are missing.
        self._addDefaultValues(configuration)
        return configuration['DEFAULT']

    def createDefaultConfigFile(self, filename):
        """
        Create a new configuration file with default values, and save it at
        filename.
        """
        # Showing warning message on console
        message = 'Configuration file not found at {fn}. Generating a new one'
        print(message.format(fn=filename), file=stderr)

        # Creating a new config file with the default values.
        config = configparser.ConfigParser()
        config['DEFAULT'] = self.getDefaultValues()

        # Writing config to the specified file.
        try:
            with open(filename, 'w') as configFile:
                config.write(configFile)
        except (IOError, Exception):
            # Could not write configuration to the file. No big deal, carry on
            pass
        return config

    def _addDefaultValues(self, config):
        '''
        Adds default values to the current configuration, to make sure no
        key is absent.
        '''
        defaultValues = self.getDefaultValues()
        for key in defaultValues:
            if key not in config['DEFAULT']:
                config.set('DEFAULT', key, str(defaultValues[key]))

    def getDefaultValues(self):
        return {
            'POLL_WAIT': 1000,
            'URL_RELOAD_TIME': 30000,
            'URL_FILENAME': 'urls.txt',
            'CONTENT_TAG': 'body',
            'DIFF': 'no',
            'DIFF_FILENAME': 'diffs/{name}_{date}',
            'VERBOSE': 'yes',
            'SHOW_ERROR_MESSAGES': 'yes',
            'TIME_FILE': 'time.txt'
        }