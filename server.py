import poll
import models
import config
import urls
import sys
from threading import Timer

if __name__ == '__main__':
    # Load configuration
    if len(sys.argv) > 1:
        configFile = sys.argv[1]
    else:
        configFile = 'config.txt'
    reader = config.ConfigurationReader()
    configuration = reader.read(configFile)

    # Create needed resources
    data = models.PollerData(urls.UrlsRetriever(), configuration)

    poller = poll.Poller(data, configuration)

    # Start polling loop
    wait = int(configuration.get('POLL_WAIT', 1000)) // 1000

    def recurring_poll():
        poller.poll_all()
        Timer(wait, recurring_poll).start()

    recurring_poll()