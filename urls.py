class UrlsRetriever:
    def getUrlList(self, config):
        """
        Retrieves the urls from the url file, as specified in the config.
        """
        urlFilename = str(config.get('URL_FILENAME', 'urls.txt'))
        urls = []
        try:
            with open(urlFilename, mode='r') as f:
                for line in f:
                    line = line.rstrip()
                    if line:
                        urls.append(line)
        except Exception:
            # Stop reading file and return what has been read up until now.
            pass
        return urls