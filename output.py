from datetime import datetime
import os


class OutputWriter:
    """
    Handles some output methods.
    Effective writing will depend on the configuration file.
    """
    def __init__(self, config):
        self.config = config

    def printDiff(self, name, content):
        """
        Create a new file for a site containing the selected content.
        This will allow multiple versions to be diff-ed to see the differences
        """
        if self.config.getboolean('DIFF'):
            filename = self.config.get(
                'DIFF_FILENAME',
                'diffs/{name}_{date}'
            ).format(
                name=name,
                date=datetime.now().strftime('%s')
            )
            if os.path.dirname(filename) \
                    and not os.path.exists(os.path.dirname(filename)):
                os.makedirs(os.path.dirname(filename))
            f = open(filename, mode='x')
            print(content, file=f)

    def printTimeFile(self, sites):
        """
        Updates the file containing the urls with their change timestamps.
        """
        line = '{site} : {times}'
        filename = self.config.get('TIME_FILE',
                                   'time.txt')
        if os.path.dirname(filename) \
                and not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))
        with open(filename, 'w') as f:
            for site in sites:
                times = [t.strftime('%s') for t in sites[site].changeTimes]
                print(line.format(site=site, times=times), file=f)

    def printNotification(self, url, time, firstTime):
        """
        Print a notification on the console, when a site is registered or
        a change has been noticed.
        """
        if self.config.getboolean('VERBOSE'):
            if firstTime:
                message = '{time} - Registered {url}'
            else:
                message = '{time} - Noticed a change on {url}'
            print(message.format(url=url, time=time))