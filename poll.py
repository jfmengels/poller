import urllib.request
from sys import stderr
from output import OutputWriter


class Parser:
    def getContent(self, htmlContent, config):
        """
        Returns the content, parsed from htmlContent.
        """
        contentTag = config.get('CONTENT_TAG', 'body').lower()
        beginIndex = htmlContent.find('<' + contentTag + '>') \
            + len(contentTag) + 2
        endIndex = htmlContent[beginIndex:].find('</' + contentTag + '>') \
            + beginIndex
        return htmlContent[beginIndex:endIndex]


class Poller:
    """
    Poller motor. Will watch the sites specified in data and poll them.
    """
    def __init__(self, data, config, parser=Parser()):
        self.data = data
        self.config = config
        self.parser = parser
        self.outputwriter = OutputWriter(config)

    def poll_all(self):
        """
        Polls all registered websites.
        If any changes has been found, the time file will get updated.
        """
        # Gather urls to poll
        urls = self.data.getSitesUrls()

        # Poll urls
        foundChange = False
        for url in urls:
            foundChange = self.poll(url) or foundChange

        if foundChange:
            self.outputwriter.printTimeFile(self.data.sites)

    def poll(self, url):
        """
        Polls the website, and find out if the content has been changed.
        If it has, it will notify the data object that will take care of
        the necessary changes.
        """
        # Get HTML resource
        try:
            resource = urllib.request.urlopen(url)
            htmlContent = str(resource.read())
            resource.close()
        # Handle exceptions
        except urllib.error.HTTPError as exc:
            if self.config.getboolean('SHOW_ERROR_MESSAGES'):
                errorMessage = "An HTTP request didn't work out\n" \
                    + "when polling {url}:\n{message}"
                print(errorMessage.format(url=url, message=str(exc)),
                      file=stderr)
            return False
        except Exception as exc:
            if self.config.getboolean('SHOW_ERROR_MESSAGES'):
                errorMessage = "An unknown error occurred\n" \
                    + "when polling {url}:\n{message}"
                print(errorMessage.format(url=url, message=str(exc)),
                      file=stderr)
            return False

        # Find the content we are interested in
        content = self.parser.getContent(htmlContent, self.config)

        # Update data if needed, and return whether a change has been made.
        return self.data.updateSite(url, content, self.outputwriter)